# Migration `20200622225103-message-user`

This migration has been generated by Matias Ribichich at 6/22/2020, 10:51:03 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql
ALTER TABLE "public"."Message" ADD FOREIGN KEY ("userId")REFERENCES "public"."User"("id") ON DELETE CASCADE  ON UPDATE CASCADE
```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20200622224327-chat..20200622225103-message-user
--- datamodel.dml
+++ datamodel.dml
@@ -2,21 +2,22 @@
 // learn more about it in the docs: https://pris.ly/d/prisma-schema
 datasource db {
   provider = "postgresql"
-  url = "***"
+  url      = env("DATABASE_URL")
 }
 generator client {
   provider = "prisma-client-js"
 }
 model User {
-  createdAt DateTime @default(now())
-  email     String   @unique
-  id        String   @default(cuid()) @id
+  createdAt DateTime  @default(now())
+  email     String    @unique
+  id        String    @default(cuid()) @id
   name      String
   chats     Chat[]
+  Message   Message[]
 }
 model Chat {
   createdAt DateTime  @default(now())
@@ -34,5 +35,6 @@
   content   String
   id        String   @default(cuid()) @id
   timestamp DateTime @default(now())
   userId    String
+  user      User     @relation(fields: [userId], references: [id])
 }
```


