import { gql } from 'apollo-server'
import { IResolvers, makeExecutableSchema } from 'graphql-tools'
import { Context } from './context'
import { Resolvers } from './types/gql'

const typeDefs = gql`
    type User {
        email: String!
        id: ID!
        name: String!
    }

    type Query {
        user(id: ID!): User
        users: [User!]!
    }

    type Mutation {
        signupUser(input: UserCreateInput!): User!
    }

    input UserCreateInput {
        email: String!
        name: String!
    }
`

const resolvers: Resolvers = {
    Query: {
        users: (parent, args, ctx) => {
            return ctx.prisma.user.findMany({})
        },
        user: (parent, { id }, ctx) => {
            return ctx.prisma.user.findOne({
                where: { id },
            })
        },
    },

    Mutation: {
        signupUser: (parent, args, ctx) => {
            return ctx.prisma.user.create({ data: args.input })
        },
    },
}

export const schema = makeExecutableSchema<Context>({
    resolvers: resolvers as IResolvers,
    typeDefs,
})
