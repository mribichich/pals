# pals

WIP platform to make new friends, practice languages with native speakers and meet travelers

## Server

nodejs graphql server written in typescript using apollo-server, prisma and postgres

## Client

Web app written in typescript using reactjs, apollo-client and material-ui

## TODO:

### Server

-   add code-gen
-   deploy to heroku

### Client

-   init with CRA
-   add code-gen
-   deploy to vercel/netlify
